'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * RequiredResource Schema
 */
var RequiredResourceSchema = new Schema({
	resource_type: {
		type: String,
		default: 'TL',
		required: 'Please fill Project type',
		trim: true
	},
	resource_level: {
		type: Number,
		default: 1
	},
	resource_amount: {
		type: Number,
		default: 1
	}
});

exports.Schema=RequiredResourceSchema;
mongoose.model('RequiredResource', RequiredResourceSchema);