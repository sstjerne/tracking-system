'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var RequiredResourceSchema=require('./required-resource.server.model.js').Schema;
// var RequiredResource = mongoose.schema('RequiredResource');

/**
 * Project Schema
 */
var ProjectSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Project name',
		trim: true
	},
	estimated_duration_days: {
		type: Number,
		required: 'Please fill Project estimated duration',
		min: 1
	},
	start_date: {
		type: Date,
		required: false
	},
	required_resources: {
		type: [RequiredResourceSchema],
		default: []
	},
	created: {
		type: Date,
		default: Date.now
	},
 	priority: {
		type: [{
			type: String,
			enum: ['HIGH', 'MEDIUM', 'LOW']
		}],
		default: 'MEDIUM'
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Project', ProjectSchema);