'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Resource Schema
 */
var ResourceSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Resource name',
		trim: true
	},
	level: {
		type: Number,
		required: 'Please fill the level'
	},
	availability_date: {
		type: Date,
		required: 'Please fill the availability date'
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Resource', ResourceSchema);