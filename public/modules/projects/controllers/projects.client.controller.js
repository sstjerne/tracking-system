'use strict';

// Projects controller
angular.module('projects').controller('ProjectsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Projects',
	function($scope, $stateParams, $location, Authentication, Projects ) {
		$scope.authentication = Authentication;
		$scope.required_resources = [];
		$scope.onlyNumbers = /^\d+$/;

		$scope.required_resources_types_list = [
		      {name:'DEV', id:4},
		      {name:'QA', id: 3},
		      {name:'TL', id: 2},
		      {name:'DM', id:1}
		    ];

		$scope.required_resources_levels_list = [
		      {name:'9', id:9},
		      {name:'10', id: 10},
		      {name:'11', id: 11},
		      {name:'12', id: 12},
		      {name:'13', id: 13},
		      {name:'14', id: 14},
		      {name:'15', id: 15}
		    ];
		
		$scope.project_priority_list = [
			  { label:'HIGH', value: 'HIGH'},
		      { label:'MEDIUM', value: 'MEDIUM'},
		      { label:'LOW', value: 'LOW'}
		    ];
		$scope.priority = $scope.project_priority_list[1];


		// Create new Project
		$scope.create = function() {
			// Create new Project object
			var project = new Projects ({
				name: this.name,
				estimated_duration_days: this.estimated_duration_days,
				start_date: this.start_date,
				required_resources : this.required_resources,
				priority : this.priority.value
			});

			// Redirect after save
			project.$save(function(response) {
				$location.path('projects/' + response._id);

				// Clear form fields
				$scope.name = '';
				$scope.estimated_duration_days = null;
				$scope.start_date = null;
				$scope.required_resources = [];
				$scope.priority = null;
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Project
		$scope.remove = function( project ) {
			if ( project ) { project.$remove();

				for (var i in $scope.projects ) {
					if ($scope.projects [i] === project ) {
						$scope.projects.splice(i, 1);
					}
				}
			} else {
				$scope.project.$remove(function() {
					$location.path('projects');
				});
			}
		};

		// Update existing Project
		$scope.update = function() {
			var project = $scope.project ;

			project.priority = $scope.priority.value;

			project.$update(function() {
				$location.path('projects/' + project._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Projects
		$scope.find = function() {
			$scope.projects = Projects.query();
		};

		// Find existing Project
		$scope.findOne = function() {
			$scope.project = Projects.get({ 
				projectId: $stateParams.projectId
			}, function(error, obj){
					for (var i = $scope.project_priority_list.length - 1; i >= 0; i--) {
						if ($scope.project_priority_list[i].label === $scope.project.priority[0] 	){
							$scope.priority = $scope.project_priority_list[i];		
						} 
					}
			});
		};

		$scope.addRequiredResource = function() {
			$scope.required_resources.push({
				resource_level : $scope.required_resource_level.name,
				resource_type : $scope.required_resource_type.name,
				resource_amount : $scope.required_resource_amount
			});

			$scope.required_resource_type = '';
			$scope.required_resource_level = '';
			$scope.required_resource_amount = 1;

		};

		$scope.removeRequiredResource = function(idx) {
			var required_resource_to_delete = $scope.required_resources[idx];
			$scope.required_resources.splice(idx, 1);
		};


		$scope.addRequiredResourceFrom = function() {
			$scope.project.required_resources.push({
				resource_level : $scope.required_resource_level.name,
				resource_type : $scope.required_resource_type.name,
				resource_amount : $scope.required_resource_amount
			});

			$scope.required_resource_type = '';
			$scope.required_resource_level = '';
			$scope.required_resource_amount = 1;

		};

		$scope.removeRequiredResourceFrom = function(idx) {
			var required_resource_to_delete = $scope.project.required_resources[idx];
			$scope.project.required_resources.splice(idx, 1);
		};


/*		$scope.addRequiredResource = function() {
		   var project = $scope.project ;
		   var required_resources = $scope.project.required_resources ;

			required_resources.$addRequiredResource(function() {
				$location.path('projects/' + project._id/required_resources);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});

		};
*/
	}
]);