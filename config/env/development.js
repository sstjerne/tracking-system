'use strict';

module.exports = {
	db: 'mongodb://root:root@ds045978.mongolab.com:45978/g4-tracking-system',
	app: {
		title: 'tracking-system - Development Environment'
	},
	facebook: {
		clientID: process.env.FACEBOOK_ID || 'APP_ID',
		clientSecret: process.env.FACEBOOK_SECRET || 'APP_SECRET',
		callbackURL: 'http://localhost:3000/auth/facebook/callback'
	},
	twitter: {
		clientID: process.env.TWITTER_KEY || 'CONSUMER_KEY',
		clientSecret: process.env.TWITTER_SECRET || 'CONSUMER_SECRET',
		callbackURL: 'http://localhost:3000/auth/twitter/callback'
	},
	google: {
/*		clientID: process.env.GOOGLE_ID || '628102277624-6a9qidf445bos90mule70ssp7g2mu4lc.apps.googleusercontent.com',
		clientSecret: process.env.GOOGLE_SECRET || 'oeLwShEwxVd5TcQKN8sMHmzp',
		callbackURL: 'http://localhost:3000/auth/google/callback'
*/		clientID: process.env.GOOGLE_ID || '628102277624-ht1k204coh24l5dsprqrdritc46ev99c.apps.googleusercontent.com',
		clientSecret: process.env.GOOGLE_SECRET || 'P4IZx-zRxGvycDC-X4WrdkUF',
		callbackURL: 'https://g4-tracking-system.herokuapp.com/auth/google/callback'
	},
	linkedin: {
		clientID: process.env.LINKEDIN_ID || 'APP_ID',
		clientSecret: process.env.LINKEDIN_SECRET || 'APP_SECRET',
		callbackURL: 'http://localhost:3000/auth/linkedin/callback'
	},
	github: {
		clientID: process.env.GITHUB_ID || 'APP_ID',
		clientSecret: process.env.GITHUB_SECRET || 'APP_SECRET',
		callbackURL: 'http://localhost:3000/auth/github/callback'
	},
	mailer: {
		from: process.env.MAILER_FROM || 'MAILER_FROM',
		options: {
			service: process.env.MAILER_SERVICE_PROVIDER || 'MAILER_SERVICE_PROVIDER',
			auth: {
				user: process.env.MAILER_EMAIL_ID || 'MAILER_EMAIL_ID',
				pass: process.env.MAILER_PASSWORD || 'MAILER_PASSWORD'
			}
		}
	}
};